package modell;

public class Governante extends Usuario{
	private String estadoQueGoverna;
	private String cargoNoGoverno;
	
	public Governante(){
		
	}
	
	public Governante(String umEstado, String umCargo){
		this.estadoQueGoverna = umEstado;
		this.cargoNoGoverno = umCargo;
	}

	public String getCargoNoGoverno() {
		return cargoNoGoverno;
	}

	public void setCargoNoGoverno(String cargoNoGoverno) {
		this.cargoNoGoverno = cargoNoGoverno;
	}

	public String getEstadoQueGoverna() {
		return estadoQueGoverna;
	}

	public void setEstadoQueGoverna(String estadoQueGoverna) {
		this.estadoQueGoverna = estadoQueGoverna;
	}
	
}
