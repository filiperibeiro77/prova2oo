package modell;

public class Usuario {
	private String nome;
	private String email;
	private String idade;
	private String profissao;
	private String cpf;
	
	public Usuario(){
		
	}
	
	public Usuario(String umNome, String umEmail, String umaIdade, String umaProfissao, String umCpf){
		this.nome = umNome;
		this.email = umEmail;
		this.idade = umaIdade;
		this.profissao = umaProfissao;
		this.cpf = umCpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getIdade() {
		return idade;
	}

	public void setIdade(String idade) {
		this.idade = idade;
	}

	public String getProfissao() {
		return profissao;
	}

	public void setProfissao(String profissao) {
		this.profissao = profissao;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

}
