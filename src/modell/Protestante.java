package modell;

public class Protestante extends Usuario{
	private String tempoDeCadastro;
	
	public Protestante(){
		
	}
	
	public Protestante(String umTempo){
		this.tempoDeCadastro = umTempo;
	}

	public String getTempoDeCadastro() {
		return tempoDeCadastro;
	}

	public void setTempoDeCadastro(String tempoDeCadastro) {
		this.tempoDeCadastro = tempoDeCadastro;
	}

}
