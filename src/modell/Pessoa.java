package modell;

public class Pessoa {
	private String nome;
	private String email;
	private String idade;
	private String profissao;
	private String cpf;
	
	public Pessoa(){
		
	}
	
	public Pessoa(String umNome, String umEmail, String umaIdade, String umaProfissao, String umCpf){
		this.nome = umNome;
		this.email = umEmail;
		this.idade = umaIdade;
		this.profissao = umaProfissao;
		this.cpf = umCpf;
	}

}
