package modell;

public class Moderador extends Usuario{
	private String dataAdmissao;
	private String apelido;
	
	public Moderador(){
		
	}
	
	public Moderador(String umaData, String umApelido){
		this.dataAdmissao = umaData;
		this.apelido = umApelido;
	}

	public String getDataAdmissao() {
		return dataAdmissao;
	}

	public void setDataAdmissao(String dataAdmissao) {
		this.dataAdmissao = dataAdmissao;
	}

	public String getApelido() {
		return apelido;
	}

	public void setApelido(String apelido) {
		this.apelido = apelido;
	}
	
	
}
