package controller;
import java.util.ArrayList;

import modell.Usuario;

public class ControleUsuario {
	String comentario;
	Usuario umUsuario = new Usuario();
	ControleUsuario umControleUsuario = new ControleUsuario();
	private ArrayList<Usuario> listaUsuarios;
	private ArrayList<String> listaComentarios;
	
	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}
	
	public String cadastrar(){
		umUsuario.getNome();
		umUsuario.getCpf();
		umUsuario.getEmail();
		umUsuario.getIdade();
		umUsuario.getProfissao();
		listaUsuarios.add(umUsuario);
		return "Usuario cadastrado com Sucesso!";
	}
	
	public String removerCadastro(){
		listaUsuarios.remove(umUsuario);
		return "Usuario Removido com Sucesso";
	}
	
	public void comentar(){
		listaComentarios.add(comentario);
	}
	
}
