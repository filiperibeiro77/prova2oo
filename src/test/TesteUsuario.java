package test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import modell.Usuario;

public class TesteUsuario {
	Usuario umUsuario = new Usuario();
	
	@Before
	public void setup() throws Exception{
		
	}
	
	@Test
	public void testNome() {
		umUsuario.setNome("Joao");
		assertEquals("Joao", umUsuario.getNome());
	}
	
	@Test
	public void testEmail() {
		umUsuario.setEmail("joao@hotmail.com");
		assertEquals("joao@hotmail.com", umUsuario.getEmail());
	}
	
	@Test
	public void testIdade() {
		umUsuario.setIdade("15 anos");
		assertEquals("15 anos", umUsuario.getIdade());
	}

}
